The Power of Fifty Bits: The new science of turning good intentions into positive results
==========================================================================================

Author: Bob Nease

ISBN: 978-0-06-240745-0

Summary Date: 6 January 2018



The Book in 3 Sentences
-----------------------

Our brains process something like 10 Mb/s of information, but can only consciously
process 50 b/s.  Changing behavior requires grabbing ahold of those 50 bits and
overcoming our brain's tendency for inertia and inattention.  Suggested tactics
include: require active choice, lock-in good intentions,
implement opt-out vs. opt-in, get into the information flow, reframe choices,
piggyback on a pleasurable experience, and carefully simplify decision processes.



Summary
-------

Further description of the tactics:

* Require active choice: basically, require a deliberate choice to be made.
  Example: interrupting the checkout process at PetSmart to ask "Would you like to
  donate $1 to homeless pets?".

* Lock-in good intentions: get a pre-commitment to the desired behavior.  Ideally,
  this is paired with a reward/consequence for following-through.  Example: signing
  a document where you *pledge* to exercise regularly rather than merely *plan*
  to workout regularly.

* Use opt-out rather than opt-in: make the default action the desired behavior,
  so that our tendency to take no action results in the desired behavior.  Example:
  making 401(k) participation default, so that employees must put forth the effort
  to opt-out.  Note that this could have legal limitations in some instances.

* Get into the information flow: figure out how to make the decision process
  be noticed and acted-upon.  This is similar to product-placement (better sales
  when front-and-center compared to peripheral), or the Burmashave advertising
  billboards along roadsides.

* Reframe choices: craft the narrative to strengthen your "sales pitch".  This is
  often paird with active choice.  Example: the words *homeless* and *pets* are
  incredibly powerful in the PetSmart example since they change the narrative from
  something impersonal to something very personal (e.g., we're not talking about
  livestock, we're talking about somebody's family member!).

* Piggybacking: combine the desired behavior with something pleasurable.  Example:
  taking medicine with a spoonful of sugar, or the addition of mint flavoring to
  toothpaste.


Other notes:

* Rule of thumb: humans have a discount-rate of ~50%.  This means a near-term
  expense/sacrifice needs to promise twice the benefit at a later date in order for
  the long-view to seem attractive.

  * Example: to make me *want* to go for a run now, the benefits of running
    (e.g., improved health) seem twice as good as the allure of staying on the
    couch eating Cheetos.

* Amazon prime is a good example of loss-aversion psychology:

  * 1-time fee (per year) bundles "losses" due to shipping costs.  It's not about
    how much we pay for shipping.  It's about paying for shipping 1x per year vs
    many times per year.
  * Each purchase reminds you that shipping is free because of your Prime membership.
    This is "enumerating gains".
  * General approach: bundle losses to reduce frequency (and ideally intensity, too)
    while enumerating gains.  Basically, a lot of incrementally-positive talk
    coupled with a single negative.  This leads to the perception that there are
    many more positives than negatives.

* There's a nice flowchart in the last chapter (Exhibit 10-1, page 149) that
  guides the selection of which 50-bit tactic to employ, based on the constraints
  of the situation.


Final comments
--------------

The book didn't "reach out and grab me", but I can't put my finger on why.  It's
a very easy read, and perhaps the seeming "obviousness" of the strategies lowers
my opinion.  The tactics, though, are very good and the book does a good job of
tying them together.



Selected References
-------------------

* Thinking, Fast and Slow (Kahneman)
